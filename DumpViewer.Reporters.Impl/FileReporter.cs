﻿using DumpViewer.Core;
using DumpViewer.Core.Attributes;
using DumpViewer.Core.Enums;
using DumpViewer.Core.Utils;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DumpViewer.Reporters.Impl
{
	[ReporterName("File")]
	internal class FileReporter : IReporter
	{
		private StringBuilder _lohReport;
		private StringBuilder _thredPoolReport;
		private StringBuilder _typeStatisticReport;
		private readonly IAppConfiguration _appConfig;

		public FileReporter(IAppConfiguration appConfig)
		{
			_appConfig = appConfig;
		}

		/// <inheritdoc />
		public async Task GenerateReportAsync()
		{
			if (!_lohReport.IsNullOrEmpty())
			{
				await ExportReportAsync(_lohReport, $"{nameof(_lohReport).Substring(1)}.log").ConfigureAwait(false);
			}

			if (!_thredPoolReport.IsNullOrEmpty())
			{
				await ExportReportAsync(_thredPoolReport, $"{nameof(_thredPoolReport).Substring(1)}.log").ConfigureAwait(false);
			}

			if (!_typeStatisticReport.IsNullOrEmpty())
			{
				await ExportReportAsync(_typeStatisticReport, $"{nameof(_typeStatisticReport).Substring(1)}.log").ConfigureAwait(false);
			}
		}

		/// <inheritdoc />
		public Task PrepareReportAsync(ILargeHeapObjectReport report)
		{
			return Task.Run(() =>
			{
				var data = report.Data;
				_lohReport = new StringBuilder(data.Count * 64 + 60);

				_lohReport.AppendLine($"======== Start of {nameof(ILargeHeapObjectReport)} ========");
				_lohReport.AppendFormat("{0,16}\t{1,16}\t{2}", "Address", "Size", "Type");
				_lohReport.AppendLine();

				for (var i = 0; i < data.Count; ++i)
				{
					_lohReport.AppendLine($"{data[i].Address,16:X16}\t{data[i].Size,16:n0}\t{data[i].Type?.Name ?? "unknown type"}");
				}

				_lohReport.AppendLine($"======== End of {nameof(ILargeHeapObjectReport)} ========");
			});
		}

		/// <inheritdoc />
		public Task PrepareReportAsync(IThreadPoolReport report)
		{
			return Task.Run(() =>
			{
				var threadPool = report.Data;

				_thredPoolReport = new StringBuilder(10480);

				_thredPoolReport.AppendLine($"======== Start of {nameof(IThreadPoolReport)} ========");
				_thredPoolReport.AppendLine($"ThreadPool: {threadPool.TotalThreads} threads (#idle = {threadPool.Idle} + #running = {threadPool.Running} | #max = {threadPool.Max})");
				_thredPoolReport.AppendLine();
				_thredPoolReport.AppendLine("Threads info:");
				_thredPoolReport.AppendFormat("{0,5}\t{1,16}\t{2,5}\t{3,4}\t{4,1}\t{5,9}\t{6}", "ThrId", "Address", "Locks", String.Empty, String.Empty, "Type", "Blocking objects");
				_thredPoolReport.AppendLine();

				foreach (var thread in threadPool.Threads)
				{
					var result = new StringBuilder(200);
					var locks = new StringBuilder(100);

					for (var i = 0; i < thread.BlockObjects.Count; ++i)
					{
						locks.AppendFormat($"{thread.BlockObjects[i].OwnerThreadId,5} ({thread.BlockObjects[i].BlockingReason} {thread.BlockObjects[i].Address,16:X16})");
					}

					result.AppendFormat("{0,5}\t{1:16}\t{2,5}\t{3,4}\t{4,1}\t{5,9}\t{6}",
					thread.ThreadId.ToString(),
					thread.Address,
					(thread.LocksCount > 0) ? thread.LocksCount.ToString("####") : String.Empty,
					!thread.IsAlive ? "Dead" : String.Empty,
					thread.AbortState == ThreadAbortState.Aborted ? "A" : (thread.AbortState == ThreadAbortState.AbortRequested ? "R" : String.Empty),
					thread.Type,
					(thread.BlockObjects.Count > 0) ? locks.ToString() : String.Empty);
					_thredPoolReport.AppendLine(result.ToString());
				}

				_thredPoolReport.AppendLine($"======== End of {nameof(IThreadPoolReport)} ========");
			});
		}

		/// <inheritdoc />
		public Task PrepareReportAsync(ITypeStatisticReport report)
		{
			return Task.Run(() =>
			{
				var statistic = report.Data;
				_typeStatisticReport = new StringBuilder(77 * (statistic.Count + 3));
				_typeStatisticReport.AppendLine($"======== Start of {nameof(ITypeStatisticReport)} ========");
				_typeStatisticReport.AppendFormat("{0,16}\t{1,10}\t{2}", "Total objects size", "Objects count", "Type");
				_typeStatisticReport.AppendLine();

				foreach (var typeStatistic in statistic)
				{
					_typeStatisticReport.AppendLine($"{typeStatistic.TotalSize,16:n0}\t{typeStatistic.Count,10}\t{typeStatistic.Type?.Name ?? "Unknown type"}");
				}

				_typeStatisticReport.AppendLine($"======== End of {nameof(ITypeStatisticReport)} ========");
			});
		}

		private async Task ExportReportAsync(StringBuilder lohReport, string fileName)
		{
			var fullPath = Path.Combine(_appConfig.FileOutputPath, nameof(FileReporter));

			if (!Directory.Exists(fullPath))
			{
				Directory.CreateDirectory(fullPath);
			}

			fullPath = Path.Combine(fullPath, fileName);

			if (File.Exists(fullPath))
			{
				File.Delete(fullPath);
			}

			using (var f = File.CreateText(fullPath))
			{
				await f.WriteAsync(lohReport.ToString()).ConfigureAwait(false);
			}
		}
	}
}
