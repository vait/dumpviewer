﻿using DumpViewer.Core;
using Microsoft.Extensions.DependencyInjection;

namespace DumpViewer.Reporters.Impl.IoC
{
	public static class Module
	{
		public static IServiceCollection WithReportersLayer(this IServiceCollection @this)
		{
			return @this
				.AddScoped<IReporter, ConsoleReporter>()
				.AddScoped<IReporter, FileReporter>()
			;
		}
	}
}
