﻿using DumpViewer.Analyzers.Impl.IoC;
using DumpViewer.Reporters.Impl.IoC;
using DumpViewer.Service.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace DumpViewer.Service
{
	internal class Program
	{
		public static Task Main(string[] args)
		{
			var initialSettings = new List<KeyValuePair<string, string>>
			{
				new KeyValuePair<string, string>("FileOutput", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)),
			};

			var host = new HostBuilder()
				.ConfigureAppConfiguration((hostContext, config) =>
				{
					config
					.AddInMemoryCollection(initialSettings)
					.AddJsonFile("appsettings.json", optional: true)
					.AddCommandLine(args);
				})
				.ConfigureServices((hostContext, services) =>
				{
					services.WithAnalyzersLayer()
					.WithReportersLayer()
					.WithServiceLayer(hostContext.Configuration.Get<AppConfiguration>())
					.BuildServiceProvider();
				})
				.Build()
				;
			return host.RunAsync(new CancellationTokenSource().Token);
		}
	}
}
