﻿using DumpViewer.Core;
using System;

namespace DumpViewer.Service
{
	internal class AppConfiguration : IAppConfiguration
	{
		/// <summary>
		/// Каталог с дампами
		/// </summary>
		public string DumpDirectory { get; set; }

		/// <summary>
		/// Генераторы отчетов, которые нужно использовать
		/// </summary>
		public string[] UsedReporters { get; set; }

		/// <summary>
		/// Путь, для сохранения всех гененируемых файлов
		/// </summary>
		public string FileOutputPath { get; set; }

		/// <summary>
		/// Время ожидания завершения копирования большого дампа
		/// </summary>
		public TimeSpan WaitingCopying { get; set; }
	}
}
