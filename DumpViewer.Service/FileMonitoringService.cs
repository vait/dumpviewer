﻿using ClrMD.Extensions;
using DumpViewer.Core;
using DumpViewer.Core.Attributes;
using DumpViewer.Core.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DumpViewer.Service
{
	internal sealed class FileMonitoringService : BackgroundService
	{
		private readonly Lazy<FileSystemWatcher> _watcher;
		private readonly AppConfiguration _appConfiguration;
		private readonly IReadOnlyCollection<IDumpAnalyzer> _analyzers;
		private readonly IReadOnlyDictionary<string, IReporter> _reporters;
		private readonly ConcurrentDictionary<string, SemaphoreSlim> _fileWatchers =
			new ConcurrentDictionary<string, SemaphoreSlim>();
		private readonly TimeSpan _waitingCopying = TimeSpan.FromMinutes(20);


		public FileMonitoringService(
			AppConfiguration appConfiguration,
			IEnumerable<IDumpAnalyzer> analyzers,
			IEnumerable<IReporter> reporters)
		{
			_watcher = new Lazy<FileSystemWatcher>();
			_appConfiguration = appConfiguration;

			_analyzers = analyzers as IReadOnlyCollection<IDumpAnalyzer>;
			_appConfiguration = appConfiguration;

			var namedList = new Dictionary<string, IReporter>(reporters.Count());
			foreach (var reporter in reporters)
			{
				namedList.Add(reporter.GetAttribute<ReporterNameAttribute>()?.Name ?? reporter.GetType().Name, reporter);
			}

			_reporters = namedList;
		}

		/// <inheritdoc />
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			var watcher = _watcher.Value;
			watcher.Path = _appConfiguration.DumpDirectory;
			watcher.Filter = "*.dmp";

			watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;

			watcher.Created += OnCreate;
			watcher.Changed += OnChange;
			watcher.IncludeSubdirectories = true;
			watcher.EnableRaisingEvents = true;

			return Task.CompletedTask;
		}

		private void OnChange(object sender, FileSystemEventArgs e)
		{
			if (_fileWatchers.TryGetValue(e.FullPath, out var signal))
			{
				signal.Release(1);
			}
		}

		private void OnCreate(object sender, FileSystemEventArgs e)
		{
			if (e.ChangeType == WatcherChangeTypes.Created)
			{
				_fileWatchers[e.FullPath] = new SemaphoreSlim(0, 2);

				Task.Run(() => ProcessFile(e.FullPath));
			}
		}

		/// <inheritdoc />
		public override void Dispose()
		{
			base.Dispose();

			if (_watcher != null && _watcher.IsValueCreated)
			{
				_watcher.Value.Dispose();
			}
		}

		private async Task ProcessFile(string fullName)
		{
			var time = _appConfiguration?.WaitingCopying ?? _waitingCopying;
			var wasCreted = await _fileWatchers[fullName].WaitAsync(time).ConfigureAwait(false)
				&& await _fileWatchers[fullName].WaitAsync(time).ConfigureAwait(false);

			_fileWatchers[fullName].Dispose();
			_fileWatchers.TryRemove(fullName, out var _);

			if (!wasCreted)
			{
				return;
			}

			if (fullName.IsNullOrWhiteSpaces() || !File.Exists(fullName))
			{
				throw new FileNotFoundException("Dump file not found");
			}

			var session = ClrMDSession.LoadCrashDump(fullName);
			var reports = new List<IAnalyzerReport>(_analyzers.Count);

			foreach (var analyzer in _analyzers)
			{
				reports.Add(analyzer.Analyze(session));
			}

			var neededReporters = _reporters
				.Where(x => _appConfiguration.UsedReporters.Contains(x.Key))
				.Select(x => x.Value)
				.ToList();

			var reporterTasks = new List<Task>(reports.Count * neededReporters.Count);

			foreach (var reporter in neededReporters)
			{
				reporterTasks.AddRange(reports.Select(reporte => reporte.AcceptAsync(reporter)));
			}

			await Task.WhenAll(reporterTasks).ConfigureAwait(false);

			reporterTasks.Clear();

			foreach (var reporter in neededReporters)
			{
				reporterTasks.Add(reporter.GenerateReportAsync());
			}

			await Task.WhenAll(reporterTasks).ConfigureAwait(false);

			session.Dispose();
		}
	}
}
