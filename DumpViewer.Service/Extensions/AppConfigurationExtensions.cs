﻿using System.IO;

namespace DumpViewer.Service.Extensions
{
	internal static class AppConfigurationExtensions
	{
		public static AppConfiguration InitializeConfiguration(this AppConfiguration appConfiguration)
		{
			if (!Directory.Exists(appConfiguration.FileOutputPath))
			{
				appConfiguration.FileOutputPath = Directory.CreateDirectory(appConfiguration.FileOutputPath).FullName;
			}
			else
			{
				appConfiguration.FileOutputPath = Path.GetFullPath(appConfiguration.FileOutputPath);
			}

			return appConfiguration;
		}
	}
}
