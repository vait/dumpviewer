﻿using DumpViewer.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DumpViewer.Service.IoC
{
	internal static class Module
	{
		public static IServiceCollection WithServiceLayer(this IServiceCollection @this, AppConfiguration appConfig)
		{
			return @this
				.AddSingleton(appConfig)
				.AddSingleton<IAppConfiguration>(appConfig)
				.AddSingleton<IHostedService, FileMonitoringService>()
			;
		}
	}
}
