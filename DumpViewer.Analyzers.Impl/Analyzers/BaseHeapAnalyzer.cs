﻿using ClrMD.Extensions;
using DumpViewer.Core;
using DumpViewer.Core.Models;
using Microsoft.Diagnostics.Runtime;
using System.Collections.Generic;

namespace DumpViewer.Analyzers.Impl.Analyzers
{
	/// <summary>
	/// Класс реализует методы обработки объектов по сегментам кучи
	/// </summary>
	internal abstract class BaseHeapAnalyzer : IDumpAnalyzer
	{
		/// <inheritdoc />
		public IAnalyzerReport Analyze(ClrMDSession clrMdSession)
		{
			return AnalyzeInternal(clrMdSession);
		}

		/// <summary>
		/// Определяет - подходит ли данный сегмент
		/// </summary>
		protected abstract bool IsNeededSegment(ClrSegment segment);

		/// <summary>
		/// Определяет - подходит ли данный объект
		/// </summary>
		protected abstract bool IsNeededObject(ulong objectAddress, ClrType objType);

		/// <summary>
		/// Формирует отчет
		/// </summary>
		protected abstract IAnalyzerReport CreateReport(IReadOnlyList<HeapObject> objects);

		private IAnalyzerReport AnalyzeInternal(ClrMDSession clrMdSession)
		{
			var runtime = clrMdSession.Runtime;
			var totalObjects = new List<HeapObject>();
			for (var i = 0; i < runtime.Heap.Segments.Count; ++i)
			{
				var segment = runtime.Heap.Segments[i];
				if (!IsNeededSegment(segment))
				{
					continue;
				}

				var objects = GetObjects(segment);
				totalObjects.Capacity += objects.Count;
				totalObjects.AddRange(objects);
			}

			return CreateReport(totalObjects);
		}

		private IReadOnlyList<HeapObject> GetObjects(ClrSegment segment)
		{
			var objects = new List<HeapObject>(100);
			for (var objectAddress = segment.GetFirstObject(out var objType); objectAddress != 0; objectAddress = segment.NextObject(objectAddress, out objType))
			{
				var size = ulong.MinValue;
				if (objType != null)
				{
					if (!IsNeededObject(objectAddress, objType))
					{
						continue;
					}
					size = objType.GetSize(objectAddress);
				}

				var heapType = objType == null ? null : new HeapType(objType.Name);
				objects.Add(new HeapObject(heapType, size, objectAddress));
			}

			return objects;
		}
	}
}
