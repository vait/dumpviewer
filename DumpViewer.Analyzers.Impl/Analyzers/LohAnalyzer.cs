﻿using DumpViewer.Analyzers.Impl.Reports;
using DumpViewer.Core;
using DumpViewer.Core.Models;
using Microsoft.Diagnostics.Runtime;
using System.Collections.Generic;
using System.Linq;

namespace DumpViewer.Analyzers.Impl.Analyzers
{
	/// <summary>
	/// Анализатор объектов LOH
	/// </summary>
	internal class LohAnalyzer : BaseHeapAnalyzer
	{
		/// <inheritdoc />
		protected override IAnalyzerReport CreateReport(IReadOnlyList<HeapObject> objects)
		{
			return new LargeObjectHeapReport(objects.OrderByDescending(x => x.Size).ToList());
		}

		/// <inheritdoc />
		protected override bool IsNeededObject(ulong objectAddress, ClrType objType)
		{
			return objType.GetSize(objectAddress) >= Constants.LOH_MIN_SIZE;
		}

		/// <inheritdoc />
		protected override bool IsNeededSegment(ClrSegment segment)
		{
			return segment.IsLarge && !segment.IsEphemeral;
		}
	}
}
