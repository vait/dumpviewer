﻿using ClrMD.Extensions;
using DumpViewer.Analyzers.Impl.Reports;
using DumpViewer.Core;

namespace DumpViewer.Analyzers.Impl.Analyzers
{
	/// <summary>
	/// Анализатор потоков (общая информация)
	/// основано на <see cref="https://github.com/chrisnas/DebuggingExtensions"/>
	/// </summary>
	internal class ThreadAnalyzer : IDumpAnalyzer
	{
		/// <inheritdoc />
		public IAnalyzerReport Analyze(ClrMDSession clrMdSession)
		{
			var runtime = clrMdSession.Runtime;
			var threadPool = runtime.ThreadPool;
			var threads = runtime.Threads;

			return new ThreadPoolReport(threadPool, threads);
		}
	}
}
