﻿using DumpViewer.Analyzers.Impl.Reports;
using DumpViewer.Core;
using DumpViewer.Core.Models;
using Microsoft.Diagnostics.Runtime;
using System.Collections.Generic;
using System.Linq;

namespace DumpViewer.Analyzers.Impl.Analyzers
{
	/// <summary>
	/// Анализатор объектов в куче
	/// </summary>
	internal class TypeStatisticAnalyzer : BaseHeapAnalyzer
	{
		/// <inheritdoc />
		protected override IAnalyzerReport CreateReport(IReadOnlyList<HeapObject> objects)
		{
			var typeStat = objects
				.GroupBy(o => o.Type?.Name ?? "Unknown Type")
				.Select(t => new HeapTypeStatistic(t.First().Type)
				{
					Count = t.LongCount(),
					TotalSize = t.Sum(o => o.Size)
				})
				.OrderByDescending(x => x.TotalSize)
				.ThenByDescending(x => x.Count);

			return new TypeStatisticReport(typeStat.ToList());
		}

		/// <inheritdoc />
		protected override bool IsNeededObject(ulong objectAddress, ClrType objType)
		{
			return objType.GetSize(objectAddress) < Constants.LOH_MIN_SIZE;
		}

		/// <inheritdoc />
		protected override bool IsNeededSegment(ClrSegment segment)
		{
			return true;
		}
	}
}
