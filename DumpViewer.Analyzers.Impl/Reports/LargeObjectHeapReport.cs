﻿using DumpViewer.Core;
using DumpViewer.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DumpViewer.Analyzers.Impl.Reports
{
	/// <inheritdoc/>
	internal class LargeObjectHeapReport : ILargeHeapObjectReport
	{
		/// <inheritdoc />
		public IReadOnlyList<HeapObject> Data { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public LargeObjectHeapReport(IReadOnlyList<HeapObject> objects)
		{
			Data = objects;
		}

		/// <inheritdoc />
		public Task AcceptAsync(IReporter reporter)
		{
			return reporter.PrepareReportAsync(this);
		}
	}
}
