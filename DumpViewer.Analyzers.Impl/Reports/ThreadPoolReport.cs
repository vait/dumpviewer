﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DumpViewer.Analyzers.Impl.Extensions;
using DumpViewer.Core;
using DumpViewer.Core.Enums;
using DumpViewer.Core.Models;
using Microsoft.Diagnostics.Runtime;

namespace DumpViewer.Analyzers.Impl.Reports
{
	/// <inheritdoc/>
	internal class ThreadPoolReport : IThreadPoolReport
	{
		/// <inheritdoc/>
		public ThreadPool Data { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public ThreadPoolReport(ClrThreadPool clrThreadPool, IList<ClrThread> clrThreads)
		{
			var threads = clrThreads.Select(thread =>
			{
				var blockObjects = thread.BlockingObjects.Select(x => new ThreadLock(x.Reason.ToString(), x.Owner?.ManagedThreadId ?? -1, x.Object)).ToList();
				return new Thread(
					thread.ManagedThreadId,
					thread.Address.ToString("X16"),
					thread.LockCount,
					thread.IsAlive,
					thread.IsAborted ? ThreadAbortState.Aborted : thread.IsAbortRequested ? ThreadAbortState.AbortRequested : ThreadAbortState.None,
					thread.GetThreadType(),
					thread.GetExceptions(),
					blockObjects);
			});

			Data = new ThreadPool(clrThreadPool.TotalThreads, clrThreadPool.IdleThreads, clrThreadPool.RunningThreads,
				clrThreadPool.MinThreads, threads.ToList());
		}

		/// <inheritdoc/>
		public Task AcceptAsync(IReporter reporter)
		{
			return reporter.PrepareReportAsync(this);
		}
	}
}
