﻿using DumpViewer.Core;
using DumpViewer.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DumpViewer.Analyzers.Impl.Reports
{
	/// <inheritdoc />
	internal class TypeStatisticReport : ITypeStatisticReport
	{
		/// <inheritdoc />
		public IReadOnlyCollection<HeapTypeStatistic> Data { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public TypeStatisticReport(IReadOnlyCollection<HeapTypeStatistic> statistic)
		{
			Data = statistic;
		}

		/// <inheritdoc />
		public Task AcceptAsync(IReporter reporter)
		{
			return reporter.PrepareReportAsync(this);
		}
	}
}
