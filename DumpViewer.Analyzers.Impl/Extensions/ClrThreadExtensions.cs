﻿using System.Collections.Generic;
using System.Text;
using DumpViewer.Core.Enums;
using DumpViewer.Core.Models;
using Microsoft.Diagnostics.Runtime;

namespace DumpViewer.Analyzers.Impl.Extensions
{
	internal static class ClrThreadExtensions
	{
		/// <summary>
		/// Определяет тип потока
		/// </summary>
		internal static ThreadType GetThreadType(this ClrThread thread)
		{
			if (thread.IsFinalizer)
			{
				return ThreadType.Finalizer;
			}

			if (thread.IsThreadpoolGate)
			{
				return ThreadType.Gate;
			}

			if (thread.IsThreadpoolCompletionPort)
			{
				return ThreadType.IO;
			}

			if (thread.IsThreadpoolTimer)
			{
				return ThreadType.Timer;
			}

			if (thread.IsThreadpoolWait)
			{
				return ThreadType.Wait;
			}

			if (thread.IsThreadpoolWorker)
			{
				return ThreadType.Worker;
			}

			return ThreadType.None;
		}

		/// <summary>
		/// Вытаскивает исключения из потока
		/// </summary>
		internal static ThreadException GetExceptions(this ClrThread thread)
		{
			if (thread.CurrentException == null)
			{
				return null;
			}

			var inners = new Stack<ClrException>();
			var ex = thread.CurrentException;
			do
			{
				inners.Push(ex);
				ex = ex.Inner;
			}
			while (ex != null);

			ex = inners.Pop();

			var result = new ThreadException(ex.Message, ex.StackTrace.StackTraceToString(), null);

			foreach (var inner in inners)
			{
				result = new ThreadException(inner.Message, inner.StackTrace.StackTraceToString(), result);
			}

			return result;
		}

		private static string StackTraceToString(this IList<ClrStackFrame> stackTrace)
		{
			var stackTraceBuilder = new StringBuilder(1000);
			foreach (var frame in stackTrace)
			{
				stackTraceBuilder.AppendLine(frame.DisplayString);
			}

			return stackTraceBuilder.ToString();
		}
	}
}
