﻿using DumpViewer.Analyzers.Impl.Analyzers;
using DumpViewer.Core;
using Microsoft.Extensions.DependencyInjection;

namespace DumpViewer.Analyzers.Impl.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection WithAnalyzersLayer(this IServiceCollection @this)
		{
			return @this
				.AddScoped<IDumpAnalyzer, ThreadAnalyzer>()
				.AddScoped<IDumpAnalyzer, LohAnalyzer>()
				.AddScoped<IDumpAnalyzer, TypeStatisticAnalyzer>()
			;
		}
	}
}
