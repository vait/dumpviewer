﻿using System;

namespace DumpViewer.Samples.LongCalculating
{
	class Program
	{
		static void Main(string[] args)
		{
			var newDate = DateTime.Now.AddMinutes(5);
			while (newDate > DateTime.Now)
			{
				for (int i = 0; i < int.MaxValue/5; ++i)
				{
					var d = i % 6;
					var x = Math.Log(i);
					var z = d * x;
					Console.WriteLine($"{i} | {d} | {x} | {z}");
				}
			}
		}
	}
}
