﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DumpViewer.Samples.LOH
{
	class Program
	{
		static void Main(string[] args)
		{
			var l = new List<byte[]>(1);

			var t = Task.Run(async () =>
			{
				for (var i = 0; i < 10; ++i)
				{
					var a = new byte[85 * 1024 * 1024 + 1024 * 10];
					for (int j = 0; j < a.Length; ++j)
					{
						a[j] = (byte)(j % byte.MaxValue);
					}

					l.Add(a);

					Console.WriteLine(i);

					await Task.Delay(5000);
				}
			});

			t.GetAwaiter().GetResult();
		}
	}
}
