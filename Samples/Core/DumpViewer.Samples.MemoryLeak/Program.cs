﻿using System;
using System.Collections.Generic;

namespace DumpViewer.Samples.MemoryLeak
{
	class Program
	{
		static void Main(string[] args)
		{
			var l = new List<List<int>>();

			for (int i = 0; i < 2000; i++)
			{
				var i65 = Math.Pow(2, 10);
				var bigArr = new List<int>((int)i65);
				for (var j = 0; j < bigArr.Capacity; ++j)
				{
					bigArr.Add(i % byte.MaxValue);
				}
				l.Add(bigArr);
			}


			Console.ReadLine();
		}
	}
}
