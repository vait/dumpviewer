﻿using System.Threading;

namespace DumpViewer.Samples.DeadLock
{
	class Program
	{
		static void Main(string[] args)
		{
			object locker1 = new object();
			object locker2 = new object();

			var t1 = new Thread(() =>
			{
				lock (locker1)
				{
					Thread.Sleep(1000);

					lock (locker2) ; // Взаимоблокировка
				}

			});

			var t2 = new Thread(() =>
			{
				lock (locker2)
				{
					Thread.Sleep(1000);

					lock (locker1) ; // Взаимоблокировка
				}

			});

			t1.Start();
			t2.Start();

			t1.Join();
			t2.Join();
		}
	}
}
