﻿using System.Threading.Tasks;

namespace DumpViewer.Core
{
	/// <summary>
	/// Методы для генерации отчета
	/// </summary>
	public interface IAnalyzerReport
	{
		/// <summary>
		/// Формирует отчет
		/// </summary>
		Task AcceptAsync(IReporter reporter);
	}
}
