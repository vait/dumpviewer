﻿namespace DumpViewer.Core
{
	/// <summary>
	/// Настройки приложения
	/// </summary>
	public interface IAppConfiguration
	{
		/// <summary>
		/// Путь, для сохранения всех гененируемых файлов
		/// </summary>
		string FileOutputPath { get; set; }
	}
}
