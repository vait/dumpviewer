﻿using DumpViewer.Core.Models;
using System.Collections.Generic;

namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс отчета по большим объектам БД
	/// </summary>
	public interface ILargeHeapObjectReport : IAnalyzerReportData<IReadOnlyList<HeapObject>>
	{
	}
}
