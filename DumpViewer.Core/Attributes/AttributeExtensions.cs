﻿using System;

namespace DumpViewer.Core.Attributes
{
	public static class AttributeExtensions
	{
		public static T GetAttribute<T>(this object _obj) where T : Attribute
		{
			var attributes = _obj.GetType().GetCustomAttributes(typeof(T), false);

			foreach (Attribute attr in attributes)
			{
				if (attr is T)
				{
					return (T)attr;
				}
			}

			return null;
		}
	}
}
