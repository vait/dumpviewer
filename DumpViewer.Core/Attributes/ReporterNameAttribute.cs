﻿using System;

namespace DumpViewer.Core.Attributes
{
	/// <summary>
	/// Атрибут генератора отчетов, указывающий имя генератора
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class ReporterNameAttribute : Attribute
	{
		/// <summary>
		/// Наименование
		/// </summary>
		public string Name { get; set; }

		public ReporterNameAttribute(string name)
		{
			Name = name;
		}
	}
}
