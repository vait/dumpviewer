﻿namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Статистика по типу
	/// </summary>
	public class HeapTypeStatistic
	{
		/// <summary>
		/// Тип
		/// </summary>
		public HeapType Type { get; set; }

		/// <summary>
		/// Количество объектов
		/// </summary>
		public long Count { get; set; }

		/// <summary>
		/// Количество объектов
		/// </summary>
		public decimal TotalSize { get; set; }

		public HeapTypeStatistic(HeapType type)
		{
			Type = type;
		}
	}
}
