﻿namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Исключение, возникшее в потоке
	/// </summary>
	public class ThreadException
	{
		/// <summary>
		/// Сообщение
		/// </summary>
		public string Message { get; }

		/// <summary>
		/// Стек
		/// </summary>
		public string StackTrace { get; }

		/// <summary>
		/// Вложенное исключение
		/// </summary>
		public ThreadException Inner { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public ThreadException(string message, string stackTrace, ThreadException inner = null)
		{
			Message = message;
			StackTrace = stackTrace;
			Inner = inner;
		}
	}
}
