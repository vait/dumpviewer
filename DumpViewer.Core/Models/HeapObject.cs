﻿namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Объект кучи
	/// </summary>
	public class HeapObject
	{
		/// <summary>
		/// Тип
		/// </summary>
		public HeapType Type { get; }

		/// <summary>
		/// Размер в байтах
		/// </summary>
		public decimal Size { get; }

		/// <summary>
		/// Адрес
		/// </summary>
		public ulong Address { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public HeapObject(HeapType type, ulong size, ulong address)
		{
			Type = type;
			Size = size;
			Address = address;
		}
	}
}
