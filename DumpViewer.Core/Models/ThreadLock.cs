﻿namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Объекты блокировки потока
	/// </summary>
	public class ThreadLock
	{
		/// <summary>
		/// Идентификатор владельца потока
		/// </summary>
		public int OwnerThreadId { get; }

		/// <summary>
		/// Причина блокировки
		/// </summary>
		public string BlockingReason { get; }

		/// <summary>
		/// Адрес в памяти блокируемого объекта
		/// </summary>
		public ulong Address { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public ThreadLock(string reason, int threadOwnerId, ulong address)
		{
			BlockingReason = reason;
			OwnerThreadId = threadOwnerId;
			Address = address;
		}
	}
}
