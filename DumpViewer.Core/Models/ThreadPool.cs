﻿using System.Collections.Generic;

namespace DumpViewer.Core.Models
{
	/// <summary>
	/// ThreadPool
	/// </summary>
	public class ThreadPool
	{
		/// <summary>
		/// Всего потоков в пуле
		/// </summary>
		public int TotalThreads { get; }

		/// <summary>
		/// Количество простаивающих потоков
		/// </summary>
		public int Idle { get; }

		/// <summary>
		/// Количество выполняемых потоков
		/// </summary>
		public int Running { get; }

		/// <summary>
		/// Максимальное количество потоков
		/// </summary>
		public int Max { get; }

		/// <summary>
		/// Потоки
		/// </summary>
		public IReadOnlyCollection<Thread> Threads { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public ThreadPool(
			int total,
			int idle,
			int running,
			int max,
			IReadOnlyCollection<Thread> threads)
		{
			TotalThreads = total;
			Idle = idle;
			Running = running;
			Max = max;
			Threads = threads;
		}
	}
}
