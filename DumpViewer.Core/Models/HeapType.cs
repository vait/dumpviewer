﻿namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Тип
	/// </summary>
	public class HeapType
	{
		/// <summary>
		/// Имя
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public HeapType(string name)
		{
			Name = name;
		}
	}
}
