﻿using DumpViewer.Core.Enums;
using System.Collections.Generic;

namespace DumpViewer.Core.Models
{
	/// <summary>
	/// Thread
	/// </summary>
	public class Thread
	{
		/// <summary>
		/// Идентификатор потока
		/// </summary>
		public int ThreadId { get; }

		/// <summary>
		/// Адрес в памяти
		/// </summary>
		public string Address { get; }

		/// <summary>
		/// Количество блокировок
		/// </summary>
		public uint LocksCount { get; }

		/// <summary>
		/// Поток активен или нет
		/// </summary>
		public bool IsAlive { get; }

		/// <summary>
		/// Состояние прерывания потока
		/// </summary>
		public ThreadAbortState AbortState { get; }

		/// <summary>
		/// Тип потока
		/// </summary>
		public ThreadType Type { get; }

		/// <summary>
		/// Заблокирован ли поток
		/// </summary>
		public bool IsLocked => LocksCount > 0;

		/// <summary>
		/// Поток мертв?
		/// </summary>
		public bool IsDead => Type == ThreadType.Worker && !IsAlive;

		/// <summary>
		/// Исключения в потоке
		/// </summary>
		public ThreadException Exception { get; }

		/// <summary>
		/// Блокирующие объекты
		/// </summary>
		public IReadOnlyList<ThreadLock> BlockObjects { get; }

		/// <summary>
		/// .ctor
		/// </summary>
		public Thread(
			int threadId,
			string address,
			uint locksCount,
			bool isAlive,
			ThreadAbortState abortState,
			ThreadType threadType,
			ThreadException threadException,
			IReadOnlyList<ThreadLock> blockObjects)
		{
			ThreadId = threadId;
			Address = address;
			LocksCount = locksCount;
			IsAlive = isAlive;
			AbortState = abortState;
			Type = threadType;
			Exception = threadException;
			BlockObjects = blockObjects;
		}
	}
}
