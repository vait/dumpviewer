﻿using System.Threading.Tasks;

namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс генератора отчетов
	/// </summary>
	public interface IReporter
	{
		/// <summary>
		/// Вызывает метод создания отчета LOH
		/// </summary>
		Task PrepareReportAsync(ILargeHeapObjectReport report);

		/// <summary>
		/// Вызывает метод создания отчета по пулу потоков
		/// </summary>
		Task PrepareReportAsync(IThreadPoolReport report);

		/// <summary>
		/// Вызывает метод создания отчета по использованию памяти
		/// </summary>
		Task PrepareReportAsync(ITypeStatisticReport report);

		/// <summary>
		/// Сформировать отчет
		/// </summary>
		Task GenerateReportAsync();
	}
}
