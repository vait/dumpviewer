﻿using System.Text;

namespace DumpViewer.Core.Utils
{
	public static class StringHelper
	{
		/// <summary>
		/// Проверяет строку на пустоту
		/// </summary>
		public static bool IsNullOrWhiteSpaces(this string str)
		{
			return str == null || str.Trim().Length == 0;
		}

		/// <summary>
		/// Проверяет StringBuilder на пустоту
		/// </summary>
		public static bool IsNullOrEmpty(this StringBuilder str)
		{
			return str == null || str.Length == 0;
		}
	}
}
