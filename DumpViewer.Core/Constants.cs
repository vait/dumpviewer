﻿namespace DumpViewer.Core
{
	public static class Constants
	{
		/// <summary>
		/// Размер объекта, с которого он попадает в LOH
		/// </summary>
		public static ulong LOH_MIN_SIZE = 85000;
	}
}
