﻿using ClrMD.Extensions;

namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс предоставляющий методы анализа дампа
	/// </summary>
	public interface IDumpAnalyzer
	{
		/// <summary>
		/// Анализирует дамп
		/// </summary>
		IAnalyzerReport Analyze(ClrMDSession clrMdSession);
	}
}
