﻿namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс расширяет свойством с данными по отчету
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IAnalyzerReportData<T> : IAnalyzerReport
	{
		/// <summary>
		/// Данные
		/// </summary>
		T Data { get; }
	}
}
