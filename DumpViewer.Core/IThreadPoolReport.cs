﻿using DumpViewer.Core.Models;

namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс отчета по пулу потоков и потокам
	/// основано на <see cref="https://github.com/chrisnas/DebuggingExtensions"/>
	/// </summary>
	public interface IThreadPoolReport : IAnalyzerReportData<ThreadPool>
	{
	}
}
