﻿using System.ComponentModel;

namespace DumpViewer.Core.Enums
{
	/// <summary>
	/// Состояние отмены потока
	/// </summary>
	public enum ThreadAbortState
	{
		[Description("")]
		None,

		[Description("A")]
		Aborted,

		[Description("R")]
		AbortRequested
	}
}
