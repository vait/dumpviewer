﻿using System.ComponentModel;

namespace DumpViewer.Core.Enums
{
	/// <summary>
	/// Тип потока
	/// </summary>
	public enum ThreadType
	{
		[Description("None")]
		None,

		[Description("IO")]
		IO,

		[Description("Worker")]
		Worker,

		[Description("Finalizer")]
		Finalizer,

		[Description("Gate")]
		Gate,

		[Description("Timer")]
		Timer,

		[Description("Wait")]
		Wait,
	}
}
