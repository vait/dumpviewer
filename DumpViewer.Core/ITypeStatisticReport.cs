﻿using DumpViewer.Core.Models;
using System.Collections.Generic;

namespace DumpViewer.Core
{
	/// <summary>
	/// Интерфейс отчета по статистики используемых типов
	/// </summary>
	public interface ITypeStatisticReport : IAnalyzerReportData<IReadOnlyCollection<HeapTypeStatistic>>
	{

	}
}

